#!/usr/bin/env python
'''
    Copyright (c) 2017 Woluxi
    File name: xmit.py
    Author: Ninad Ghodke
    Date created: 4/10/2017
    Python Version: 2.7
    Description: This module transmits the data via the LED according to the protocol.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import time
import binascii

_SLEEP_TIME = 0.250

class BitBlink(object):
   def blink(self):
       pass

class Xmit (object):
    last_time_sent = int(round(time.time() * 1000))
    hw_id = 0x30
    mode = 0x30
    vendor_id = bytearray(b'\x02\x2A')

    def xmit_bytes(self, data):
        """
        """
        #print("inside byte representation")
        self._xmit_preamble()
        length = len(data) - 1
        self._xmit_meta_data(length)
        if ((0x30 & self.mode) != 0x20) :
            self._xmit_vendor_id()
            self._xmit_hw_id()
        for index in range(0, len(data)):
            #protocol says transmit the byte twice if equal to preamble.
            if (data[index] == 0xf0):
                self._xmit_data_byte(ord(data[index]))
            self._xmit_data_byte(ord(data[index]))

    def _xmit_bit(self, bit):
        time_sent = int(round(time.time() * 1000))
        drift = (time_sent - self.last_time_sent) - 250
        self.bl.blink(bit)
        time_now = int(round(time.time() * 1000))
        time_diff = (250 - (time_now - self.last_time_sent))/1000.0
        time_diff = ((time_now - time_sent))/1000.0
        time.sleep(_SLEEP_TIME - time_diff )
        self.last_time_sent = time_sent

    def _xmit_data_byte(self, data):
        #start bit
        self._xmit_bit(1)
        dt = format(data, 'b').zfill(8)
        #print dt
        for b in dt:
            self._xmit_bit(b)
        #stop bit
        self._xmit_bit(0)
        
    def _xmit_vendor_id(self):
        print "inside vendor id " + format(self.vendor_id[0], '#04x') + " " \
              + format(self.vendor_id[1], '#04x')
        self._xmit_data_byte(self.vendor_id[0])
        self._xmit_data_byte(self.vendor_id[1])

    def _xmit_hw_id(self):
        print "inside hw id " + format(self.hw_id, '#04x')
        self._xmit_data_byte(self.hw_id)

    def _xmit_meta_data(self, len):
        d = self.mode | len
        #print "inside meta data " +format(d, '#04x')
        self._xmit_data_byte(d)

    def _xmit_preamble(self):
        d = 0xf0
        #print "inside preamble " + format(d, '#04x')
        self._xmit_data_byte(d)
            
    def xmit_chr(self, data):
        print("inside char representation")

    def xmit_int(self, data):
        print("inside int representation")

    def __init__(self,bl):
        self.bl = bl
        self.map = {
            int : self.xmit_int,
            chr: self.xmit_chr,
            bytes: self.xmit_bytes
        }


    def xmit_data(self, data):
        return self.map[type(data)]( data)
