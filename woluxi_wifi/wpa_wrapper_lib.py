#!/usr/bin/env python
'''
    Copyright (c) 2017 Woluxi
    File name: wpa_wrapper_lib.py
    Author: Sandeep Nirmale
    Date created: 4/10/2017
    Date last modified: 8/25/2017
    Python Version: 2.7
    Description: This is the wpa_cli helper library 
         This module implements calls required to adding networks to wpa_supplicant.conf file.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import subprocess
from subprocess import call, check_output
import logging
import logging.handlers
import json

log = logging.getLogger('wifi_provision')
log.setLevel(logging.DEBUG)

WPA_SUCCESS=0
WPA_ERROR_UNKNOWN=1
WPA_ERROR_DISCONNECTED=2
WPA_ERROR_DISCONNECTED_WRONG_PW=3

def wpa_add_network():
    out = check_output(['/sbin/wpa_cli', '-i', 'wlan0', 'add_network'])
    id = -1
    if "FAIL" in out:
        return id
    for line in out.splitlines():
        if "Selected interface" in line:
            continue
        id = int(line)
    return id

def wpa_remove_network(nw_id):
    out = check_output(['/sbin/wpa_cli', '-i', 'wlan0', 'remove_network', str(nw_id)])
    if "FAIL" in out:
        return False
    return True

def wpa_enable_network(nw_id):
    out = check_output(['/sbin/wpa_cli', '-i', 'wlan0', 'enable_network', str(nw_id)])
    if "FAIL" in out:
        log.error("enable_network failed with " + out)
        return False
    return True

def wpa_set_network(nw_id, attr, value):
    out = check_output(['/sbin/wpa_cli', '-i', 'wlan0', 'set_network', str(nw_id), attr, value])
    if "FAIL" in out:
        log.error("set_network failed with " + out)
        return False
    return True

def wpa_save_config():
    out = check_output(['/sbin/wpa_cli', '-i', 'wlan0', 'save_config'])
    if "FAIL" in out:
        log.error("save_config failed with " + out)
        return False
    return True

"""
 Check the status of wireless network after enable network.
"""
def wpa_check_status(enable_time):
    ret = WPA_ERROR_UNKNOWN
    proc = subprocess.Popen(['sudo','/bin/journalctl', '_COMM=wpa_supplicant', '--since', enable_time, '-o', 'json'], stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        jdata = json.loads(line)
        if 'CTRL-EVENT-SSID-TEMP-DISABLED' in jdata['MESSAGE']:
            #the real code does filtering here
            if 'reason=WRONG_KEY' in  jdata['MESSAGE']:
                log.error("ERROR: Incorrect wifi password!")
            ret = WPA_ERROR_DISCONNECTED_WRONG_PW
            break
        elif 'CTRL-EVENT-CONNECTED' in jdata['MESSAGE']:
            ret = WPA_SUCCESS
            break
        elif 'CTRL-EVENT-DISCONNECTED' in jdata['MESSAGE']:
            log.error("ERROR: Enable network FAILED")
            ret = WPA_ERROR_DISCONNECTED
            continue
        else:
            continue
    if ret == WPA_ERROR_UNKNOWN:
        log.error("WARNING: Unknown error with wpa")
    return ret

"""
 Get the status of wireless lan.
 Return: Various wlan attributes as dictionary with key-value pairs
"""
def wpa_get_status():
    out = check_output(["/sbin/wpa_cli", '-i', 'wlan0', "status"])
    status = {}
    for i in out.split('\n'):
        # Skip lines with no key-value pair
        if "=" not in i :
            continue
        splitted=i.split("=")
        status[splitted[0].strip()]=splitted[1].strip()
    return status

def wpa_configure_network(ssid, psk):
    #log.debug("Configuring WiFi network    %s %s" % (ssid, psk))
    nw_id = wpa_add_network()
    if (id == -1):
        return -1
    ssid_str = "\"%s\"" % ssid
    if not wpa_set_network(nw_id, 'ssid', ssid_str):
        log.error("ERROR: set network failed for ssid %s" % ssid_str)
        return -1
        
    # If psk length is zero, then no need to add psk details to wpa
    if len(psk) > 0:
      psk_str = "\"%s\"" % psk
      if not wpa_set_network(nw_id, 'psk', psk_str):
          log.error("ERROR: set network psk failed for ssid %s" % ssid_str)
          return -1
    else:
      if not wpa_set_network(nw_id, 'key_mgmt', 'NONE'):
          log.error("ERROR: set network key_mgmt failed for ssid %s" % ssid_str)
          return -1

    return nw_id

def wpa_start_network(nw_id):
    #log.debug("Enabling the network %d" % nw_id)
    if not wpa_enable_network(nw_id):
        log.error("ERROR: Enable network failed for network id %s" % nw_id)
        return False

    if not wpa_save_config():
        log.error("ERROR: Save network config failed for when enabling nw id %s" % nw_id)
        return False

    return True

def wpa_unconfigure_network(nw_id):
    #log.debug("Removing network configuration %d" % nw_id)
    if not wpa_remove_network(nw_id):
        log.error("ERROR: Remove network failed for network id %s" % nw_id)
        return False

    if not wpa_save_config():
        log.error("ERROR: Save network config failed for when removing  nw id %s" % nw_id)
        return False

    return True

def connect_network(nw_id):
    out = check_output(['/sbin/wpa_cli', '-i', 'wlan0', 'select_network', nw_id])
    #log.debug("select nw: " + out)
