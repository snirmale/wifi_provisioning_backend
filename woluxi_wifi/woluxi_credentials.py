#!/usr/bin/env python

'''
    Copyright (c) 2017 Woluxi
    File name: woluxi_credentials.py
    Author: Sandeep Nirmale
    Date created: 4/10/2017
    Python Version: 2.7
    Description: This file creates the credentials that the device generates
                 and smartphone would needs for verification.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import random
import time
import subprocess
import math
import logging
import logging.handlers
import os
from config import CONFIG

log = logging.getLogger('wifi_provision')
WOLUXI_SSID_FILE = "/etc/woluxi.d/woluxi.ssid"

class WoluxiCredentials():

    def __init__(self):
        self.ap_ssid = None
        self.ap_ssid_num = None
        self.ap_pw = None
        self.ap_pw_num = None
        self.web_pw = None
        self.web_pw_num = None

    def create_random(self, seed, len):
        """ Create random number
            Args:
                seed : Seed for the random number
                len : Length for the random in bytes
            Returns:
                Integer, generated random number
            Raises: None
        """

        random.seed(seed)
        num = random.randint(0,65535)

        max = math.pow(2,8*len) - 1
        random.seed(num ^ int(time.time()))
        num = random.randint(0,max)
        return num

    def recreate_web_pw(self, len):
        """ ReGenerate random number for Web Password, convert to hex in
            required length.
        Args:
            len : Length of password
        Returns:
            String, generated web password
        """
        self.web_pw_num = self.create_random(hash("WS_PW"), len)
        str_format = "%s0%dx" % ("%", 2 * len)
        str = str_format % self.web_pw_num
        self.web_pw = "%s%s" % (CONFIG['Security']['WebPwPrefix'], str)

    def create_rasp_credentials(self, len):
        """ Create credentials for Web Server and Access Point.
            Also creates or reuses the previous ssid for Access Point.
        Args:
            len : Length of web server password
        Returns: None
        """
        self.web_pw_num = self.create_random(hash("WS_PW"), len)
        self.ap_pw_num = self.create_random(hash("AP_PSK"), 1)
        str_format = "%s0%dx" % ("%", 2 * CONFIG['Security']['ApPskLen'])
        str = str_format % self.ap_pw_num
        self.ap_pw = "%s%s" % (CONFIG['Security']['ApPwPrefix'], str) 

        # Generate random number for Web Password, convert to hex in required length
        self.web_pw_num = self.create_random(hash("WS_PW"), len)
        str_format = "%s0%dx" % ("%", 2 * len)
        str = str_format % self.web_pw_num
        self.web_pw = "%s%s" % (CONFIG['Security']['WebPwPrefix'], str) 

        #log.debug("Checking ssid file")
        ssid = None
        try:
        # Generate random number for SSID, convert to hex in required length
            if os.path.exists(WOLUXI_SSID_FILE):
                #log.debug("Reading ssid file")
                sf = file(WOLUXI_SSID_FILE,'r')
                ssid = sf.read().strip()
                sf.close()
        except IOError:
            log.error("Read failed: ssid file")
            ssid = None
        except:
            log.error("Read failed: ssid file")

        if ssid:
            log.info("Using the pre-existing ssid %s" % ssid)
            self.ap_ssid = ssid
        else:
            self.ap_ssid_num = self.create_random(self.get_serialnum(), CONFIG['Security']['ApSsidLen'])
            str_format = "%s0%dx" % ("%", 2*CONFIG['Security']['ApSsidLen'])
            str = str_format % self.ap_ssid_num
            self.ap_ssid = "%s%s" % (CONFIG['Security']['ApSsidPrefix'], str) 
            log.info("Created new ssid %s" % self.ap_ssid)
            try:
                sf = file(WOLUXI_SSID_FILE,'w+')
                sf.write("%s\n" % self.ap_ssid)
                sf.close()
            except:
                log.error("ERROR: Write to ssid file failed")
                e = sys.exc_info()[0]
                log.info("Unexpected error: %s" % e)

        #log.debug("Credentials: apid=%s psk=%s wpw=%s" % (self.ap_ssid, self.ap_pw, self.web_pw))

    def get_serialnum(self):
        """ Returns the serial number of local device. """
        output = subprocess.check_output("cat /proc/cpuinfo | grep \"Serial\" | awk \'{print $3}\'", shell=True)
        return output
