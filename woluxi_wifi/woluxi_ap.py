#!/usr/bin/env python

'''
    Copyright (c) 2017 Woluxi
    File name: woluxi_ap.py
    Author: Ninad Ghodke
    Date created: 4/10/2017
    Python Version: 2.7
    Description:This file sets up the dnsmasq and the hostapd processes.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import glob
import os
import tempfile
import shutil
from shutil import copyfile
import logging
import logging.handlers
from config import CONFIG

log = logging.getLogger('wifi_provision')
DNSMASQWOLUXI="/etc/dnsmasq.d/dnsmasq.woluxi"

def configure_and_start_ap(ssid, pw, wlan):
    """ Configures and starts access point & dnsmasq service.
        Args:
            ssid: SSID of temporary access point
            pw: Password/Passphrase/Key of temporary access point
            wlan: Wireless lan interface name
        Returns: None
        Raises: None
    """

    #create a temp directory
    dirpath = tempfile.mkdtemp(prefix='woluxi_tmp')
    etcpath = os.path.dirname(os.path.realpath(__file__))
    #log.debug("tempdir %s, etcpath %s" % (dirpath, etcpath))

    copyfile(etcpath+"/etc/hostapd.conf" , dirpath+"/hostapd.conf")
    copyfile(etcpath+"/etc/dnsmasq.conf" , dirpath+"/dnsmasq.conf")
    hostapdfile = dirpath+"/hostapd.conf"
    #log.debug("Configuring AP: ssid=%s pw=%s" % (ssid, pw))

    # Update wlan interface in hostpad conf file
    cmd = "sed -i 's/INTERFACE_NAME/interface=%s/' %s" % (wlan, hostapdfile)
    os.system(cmd)

    # Update ssid in hostpad conf file
    cmd = "sed -i 's/SSID_NAME/ssid=%s/' %s" % (ssid, hostapdfile)
    os.system(cmd)

    if pw == None:
        cmd = "sed -i 's/.*wpa/#wpa/' %s" % hostapdfile
        os.system(cmd)
    else:
        cmd = "sed -i 's/.*wpa_passphrase=.*/wpa_passphrase=%s/' %s" % (pw, hostapdfile)
        os.system(cmd)
    start_ap_dnsmasq(dirpath, wlan)

def start_ap_dnsmasq(dirpath, wlan):
    """ Starts access point & dnsmasq service with Woluxi settings
        Args:
            dirpath: SSID of temporary access point
            wlan: Wireless lan interface name
        Returns: None
        Raises: None
    """

    log.info("Starting Woluxi Access Point and Dnsmasq service")

    #Stop dnsmasq & hostapd service if already running
    os.system("service dnsmasq stop")
    os.system("pkill dnsmasq")
    os.system("pkill hostapd")

    # Assign static IP to wlan interface in access point mode
    ip = "192.168.173.1"
    os.system("ifconfig %s %s netmask 255.255.255.0 up" % (wlan, ip))
    os.system("/usr/sbin/dnsmasq -C " + dirpath+"/dnsmasq.conf")
    os.system("/usr/sbin/hostapd " + dirpath+"/hostapd.conf -B")

def stop_ap_dnsmasq(reset_wlan, wlan):
    """ Stops access point & dnsmasq service with temp file cleanup
        Args:
            reset_wlan: If true, wlan interface is reset especially from
                static ip mode.
            wlan: Wireless lan interface name
        Returns: None
        Raises: None
    """

    log.info("Stopping Woluxi Access Point and Dnsmasq")
    os.system("pkill hostapd")
    os.system("pkill dnsmasq")

    # Reset the static IP access point mode for wlan interface
    if reset_wlan:
        os.system("ifconfig %s 0.0.0.0" % wlan)

    listing = glob.glob('/tmp/woluxi_tmp*')
    for d in listing:
        #log.debug("deleting directory " + d)
        shutil.rmtree(d)
