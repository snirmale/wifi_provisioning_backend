#!/usr/bin/env python

'''
    Copyright (c) 2017 Woluxi
    File name: wifi_provisioning.py
    Author: Sandeep Nirmale
    Date created: 4/10/2017
    Date last modified: 8/25/2017
    Python Version: 2.7
    Description: This is the entry point for the wifi Provisioning process using
       the woluxi mechanism.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import argparse
import time
import os
import subprocess
import signal
import threading
import BaseHTTPServer
import BaseHTTPServer
import json
import sys
import woluxi_credentials
from woluxi_credentials import WoluxiCredentials
from woluxi_web_server import WebServer
import woluxi_ap as wap
from xmit import Xmit
from xmit import BitBlink
from raspberry_blink import RaspberryBlink
from raspberry_zero_blink import RaspberryZeroBlink
import logging
import logging.handlers
from subprocess import call
from config import *
from woluxi_utils import *

log = logging.getLogger('wifi_provision')
log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
log.addHandler(handler)

FNULL = open(os.devnull, 'w')
httpd = None

class XmitBlink():
    """ Wrapper class to blink the LED with Woluxi protocol given input data
    Attributes:
        xm: Platform/hardware specific class instance to blink LED
    """

    xm = None
    def _get_revision(self):
        """ Gets the identifier (revision) of local hardware. """
        command = "cat /proc/cpuinfo"
        all_info = subprocess.check_output(command, shell=True).strip()
        for line in all_info.split("\n"):
           if "Revision" in line:
               return line
        return ""

    def __init__(self):
        """ Inits xm object based on local hardware. """
        revision_string = self._get_revision()
        #determine the class to use for blinking.
        if "9000c1" in revision_string :
            self.xm = Xmit(RaspberryZeroBlink())
        else :
            self.xm = Xmit(RaspberryBlink())

    def xmit_blink(self, data, format):
        """ Wrapper function to blink LED given the input data
            Args:
                data: Input data which is emitted through LED blinks
                format: Credential format (FORMAT_PW or FORMAT_PSK_PW)
            Returns: None
            Raises: None
        """

        if format == FORMAT_PSK_PW:
            self.xm.mode = 0x28
        else:
            self.xm.mode = 0x20
        self.xm.xmit_data(bytes(data))

def _start_provision(pw_len, format, refresh):
    """ Controls overall WiFI provisioing
        This routine creates credentials to be emitted through LED blinks. It
        starts the Woluxi temporary access point and dnsmasq service. It also
        starts the woluxi web server and then starts emitting credentials through
        LED blinks in a loop.
        Args:
            pw_len: Length of the web server password
            format: Mode of credential emitted through LED blinks
                    (FORMAT_PSK_PW or FORMAT_PW)
            refresh: Web server Credential/password is changed at this
                     interval in seconds
        Returns: None
        Raises:
            Exception if fails to create credentials.
            Exception if Keyboard interupt is received.
    """

    try:
        wcr = WoluxiCredentials()
        wcr.create_rasp_credentials(pw_len)
    except Exception, e:
        log.error("ERROR: Credential creation failed: %s" % str(e))
        e = sys.exc_info()[0]
        log.info("CRE: Unexpected error: %s" % e)

    if format == FORMAT_PSK_PW:
        ap_pw = wcr.ap_pw
    else:
        ap_pw = None

    # Start the access point
    wap.configure_and_start_ap(wcr.ap_ssid, ap_pw,
                               CONFIG['General']['WlanInterface'])

    # Update the web & ap password
    WebServer.web_pw_num = wcr.web_pw_num
    WebServer.web_pw_len = pw_len
    if format == FORMAT_PSK_PW:
        WebServer.ap_pw_num = wcr.ap_pw_num

    init_keypair()
    # Start the web server
    server_class = BaseHTTPServer.HTTPServer
    host = CONFIG['General']['WebServerIp']
    port = CONFIG['General']['WebServerPort']
    httpd = server_class((host, port), WebServer)
    log.info("Woluxi Web Server Started")

    thread = threading.Thread(target = httpd.serve_forever)
    thread.daemon = True

    try:
        thread.start()
    except KeyboardInterrupt:
        httpd.server_close()
        log.error("Woluxi Web Server Stopped")
        wap.stop_ap_dnsmasq(False, CONFIG['General']['WlanInterface'])
        sys.exit(0)

    data = createByteArray(wcr, format, pw_len)

    # Start trasnmitting blink codes
    xb = XmitBlink()
    stime = time.time()

    log.info("Started emitting Woluxi blink codes.")
    max_iterations =  CONFIG['General']['MaxBlinkIteration']
    for x in range(0, max_iterations):
        #log.debug(" Iteration %s: ssid=%s psk=%s wpw=%s" % (str(x), wcr.ap_ssid, wcr.ap_pw, wcr.web_pw))
        xb.xmit_blink(data, format)
        ctime = time.time()

        # Check if WiFi is configured
        if WebServer.wifi_configured:
            if (WebServer.nw_id < 0):
                log.error("ERROR: Network id is not set")
            #log.debug("DEBUG: Found wifi configured in main")
            cfg_data = enable_wifi()

            # Reset wifi configured flag once it is configured
            WebServer.wifi_configured = False

            # If provisioned, start IP multicast with green light indication
            if cfg_data != None:
                WebServer.nw_id = -1
                xb.xm.bl.blink(1)
                multi_cast_ip(cfg_data)
                xb.xm.bl.blink(0)

                # For Pi Zero, keep led ON
                revision_string = xb._get_revision()
                if "9000c1" in revision_string :
                    xb.xm.bl.blink(1)
                break
            else:

                # Restart the access point
                wap.configure_and_start_ap(wcr.ap_ssid, ap_pw,
                                           CONFIG['General']['WlanInterface'])

        # Is it time to refresh the credentials?
        #log.debug("stime=%d ctime=%d diff=%d wcfg=%r " % (stime, ctime, (ctime-stime), WebServer.wifi_configured))
        if (ctime - stime) > refresh:
            #Refresh the credentials
            refresh_credentials(wcr, pw_len)
            stime = ctime
            data = createByteArray(wcr, format, pw_len)

    # Stop the web server
    if httpd:
        log.info('Closing httpd')
        httpd.shutdown()
        httpd.server_close()

    if (x == (max_iterations-1)):
        wap.stop_ap_dnsmasq(False, CONFIG['General']['WlanInterface'])
        log.info('Exiting Woluxi....')
        sys.exit(0)

def check_if_connected(log_it):
    """ Checks if device is network connected or not
        Args:
            log_it: Indicated when to log, log only once and not periodically.
        Returns:
            True: If device already has network connectivity
            False: If device has no network connectivity (verfied at lease 2 times)
        Raises: None
    """

    if log_it:
        log.info('Checking connectivity..')
    # Verify connectivity 2 times with a delay
    for i in range(0, 2):
        out = subprocess.Popen("ip route show 0.0.0.0/0",shell=True, stdout=subprocess.PIPE).communicate()[0]
        for l in out.split("\n"):
            if (len(l)> 0):
                gw = l.split()[2]
                if (check_ping(gw)):
                    return True
        time.sleep(1)
    return False

def check_ping(host):
    """ Enables the ssh interface.
        Args:
            host: Name or ip address of host to be pinged.
        Returns:
            True: If host is ping-able
            False: If host is not ping-able
        Raises: None
    """

    data = subprocess.Popen("ping -c 1 " + host, shell=True, stdout=FNULL, stderr= FNULL)
    response = data.wait()
    if response == 0:
        return True
    else:
        return False

def start_provision():
    """ Wrapper function to start wifi provisioing with some config settings."""

    keylen = CONFIG['Security']['Keylen']
    sec_mode = CONFIG['Security']['Mode']
    refresh = CONFIG['Security']['KeyRefreshTime']

    _start_provision(keylen, sec_mode, refresh)
    log.info("finished start_provision")

def signal_handler(signal, frame):
    """ Handles the signal for clean shutdown. """

    if httpd:
        httpd.shutdown()
        httpd.server_close()
    wap.stop_ap_dnsmasq(False, CONFIG['General']['WlanInterface'])
    log.info('Exiting Woluxi....')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

## MAIN PROGRAM
if __name__ == '__main__':

    if (os.getuid() != 0) :
        log.error("Need to run it as root!")
        sys.exit(2)

    log.info("Woluxi wifi provisining started.")
    if not os.path.exists(CONFIG_DIR):
        os.makedirs(CONFIG_DIR)

    # Read the config file
    load_config_file()

    # If system is just started, then have enough delay before Woluxi
    # WiFI provisioning start. This will give enough time for wlan interface
    # to get IP address if device is already in provisioned state.
    uptime =float(subprocess.check_output("cat /proc/uptime", shell=True).split()[0])
    if uptime < CONFIG['General']['BootTime']:
        time.sleep(CONFIG['General']['DelayBoot'])

    keylen = CONFIG['Security']['Keylen']
    sec_mode = CONFIG['Security']['Mode']

    log_it = True
    # Forever loop
    while True:
        #if connected sleep and recheck
            if(check_if_connected(log_it)):
                if log_it:
                    log.info("No need for wifi provisining since network is present")
                log_it = False
                time.sleep(CONFIG['General']['DelayConnectivityCheck'])
            else:
                start_provision()
                log_it = True
