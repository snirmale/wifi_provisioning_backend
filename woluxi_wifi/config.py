#!usr/bin/env python
'''
    Copyright (c) 2017 Woluxi
    File name: config.py
    Author: Sandeep Nirmale
    Date created: 10/23/2017
    Date last modified: 10/23/2017
    Python Version: 2.7
    Description: This module reads the woluxi configuration file and also
                 defines the default values.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from ConfigParser import SafeConfigParser
import os

CONFIG_DIR = "/etc/woluxi.d"
DEFAULT_CONFIG_FILE = CONFIG_DIR + "/wifi_provisioning.conf"

WEB_SERVER_IP = '192.168.173.1'
WEB_SERVER_PORT = 9090

DEFAULT_KEYLEN = 2
FORMAT_PW = 0
FORMAT_PSK_PW = 1
WEB_PW_PREFIX = "Woluxi_"
AP_PW_PREFIX = "Woluxi_"
AP_SSID_PREFIX = "Woluxi_"
AP_SSID_LEN = 6
AP_PSK_LEN = 1

BOOT_TIME = 360
DELAY_BOOT = 60
KEY_REFRESH_TIME = 60   # in seconds
DELAY_AFTER_WPA_ENABLE = 20
DELAY_AFTER_WLAN_RESTART = 60
MAX_BLINK_ITERATIONS = 360
DELAY_CONNECTIVITY_CHECK = 60   # Connection check frequency every minute when provisioned
DEFAULT_WLAN_INTERFACE = 'wlan0'

def get_config_file():
    return os.environ.get('WOLUXI_CONFIG_FILE', DEFAULT_CONFIG_FILE)

CONFIG_FILE = get_config_file()
CONFIG = {}

def load_config_file(config_file=CONFIG_FILE):
    """ Load config settings using defaults and CONFIG_FILE
        Values in the config file override the defaults value.
        This is called at very start of the program to load config settings
        only one time. All config settings are loaded into a dictionary,
        CONFIG which is accessed by all other modules.
        Args:
            config_file: A file which contains Woluxi settings
        Returns: None
    """
    parser = SafeConfigParser()
    parser.read(config_file)

    #Init
    CONFIG['Security'] = {}
    CONFIG['General'] = {}

    #Load general defaults
    CONFIG['General']['MaxBlinkIteration'] = MAX_BLINK_ITERATIONS
    CONFIG['General']['DelayConnectivityCheck'] = DELAY_CONNECTIVITY_CHECK
    CONFIG['General']['WlanInterface'] = DEFAULT_WLAN_INTERFACE
    CONFIG['General']['WebServerIp'] = WEB_SERVER_IP
    CONFIG['General']['WebServerPort'] = WEB_SERVER_PORT
    CONFIG['General']['BootTime'] = BOOT_TIME
    CONFIG['General']['DelayBoot'] = DELAY_BOOT

    #Load security defaults
    CONFIG['Security']['Keylen'] = DEFAULT_KEYLEN
    CONFIG['Security']['Mode'] = FORMAT_PW
    CONFIG['Security']['KeyRefreshTime'] = KEY_REFRESH_TIME
    CONFIG['Security']['ApSsidPrefix'] = AP_SSID_PREFIX
    CONFIG['Security']['ApSsidLen'] = AP_SSID_LEN
    CONFIG['Security']['ApPskLen'] = AP_PSK_LEN
    CONFIG['Security']['ApPwPrefix'] = AP_PW_PREFIX
    CONFIG['Security']['WebPwPrefix'] = WEB_PW_PREFIX

    # Now override the defaults with values in  configuration file
    for section in parser.sections():
        for key, val in parser.items(section):
            CONFIG[section][key] = val

