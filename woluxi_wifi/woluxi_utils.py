#!/usr/bin/env python

'''
    Copyright (c) 2017 Woluxi
    File name: wifi_provisioning.py
    Author: Sandeep Nirmale
    Date created: 25/10/2017
    Python Version: 2.7
    Description: This file contains the generic utility modules.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from config import *
import wpa_wrapper_lib
from wpa_wrapper_lib import *
import time
import woluxi_credentials
from woluxi_web_server import WebServer
import logging
import logging.handlers
import woluxi_ap as wap
import datetime
import socket
import sys
from Crypto.PublicKey import RSA
import base64
from base64 import b64decode

ANY = "0.0.0.0"
SENDERPORT=1501
MCAST_ADDR = "224.0.0.1"
MCAST_PORT = 1600
MCAST_TRIES = 30

log = logging.getLogger('wifi_provision')

def addToByteArray(ba, credential, len):
    """ Configures and starts access point & dnsmasq service.
        Args:
            ba: SSID of temporary access point
            credential: Either web server password or psk key for access point
            len: Length of the credential
        Returns: None
    """
    for i in range(0, len):
        shift = (len-1-i) * 8
        mask = 0xff << shift
        byte = (credential & mask) >> shift
        ba.append(byte)

def createByteArray(wcr, format, pw_len):
    """ Create a bytearray with credentials to be emitted via LED
        Args:
            wcr: Credentials
            format: Format of credentials, FORMAT_PW or FORMAT_PSK_PW
            pw_len: Length of the web server password
        Returns:
            ba: Bytearray containing the credentials
    """

    # Init the byte array
    ba = bytearray(b'')

    if format == FORMAT_PSK_PW:
        # Add PSK to data bytearray
        addToByteArray(ba, wcr.ap_pw_num, CONFIG['Security']['ApPskLen'])

    # Add web PW to bytearray
    addToByteArray(ba, wcr.web_pw_num, pw_len)

    return ba

# This is used to restart the wireless interface (wlan) only if IP address
# does not get assigned for some reason.
def restart_wlan(enable_time):
    """ Check and restart the wlan if needed
        It used wpa cli wrapper module to track the IP assginment to wlan
        interface with retry and small delay.
        Args:
            enable_time: The time when wlan interface was enabled via wpa_cli
        Returns:
           None: Returns None if wlan interface is not successfully enabled or
                 provisioned.
           date: JSON, ip and mac address is returned if successfully enabled
                 or provisioned.
    """

    #log.debug("Checking for enable result")
    delay_cnt = 0
    wpa_state = None
    while (delay_cnt < DELAY_AFTER_WPA_ENABLE):
        time.sleep(2)
        ret = wpa_check_status(enable_time)
        if (ret == WPA_ERROR_DISCONNECTED_WRONG_PW): 
            log.error("Wrong WiFi password error")
            wpa_unconfigure_network(WebServer.nw_id)
            return None

        # Now check if IP is assigned or not
        status = wpa_get_status()
        if status.get('wpa_state'):
            wpa_state = status['wpa_state']
        if (wpa_state == "COMPLETED") and (status.get('ip_address')):
            break
        delay_cnt = delay_cnt + 2

    #log.debug("Checking enable status done")

    # If no IP assigned, restart wlan interface
    if (wpa_state != "COMPLETED") or (not status.get('ip_address')):
        log.info("Restarting wlan")
        call(['/sbin/ifconfig', CONFIG['General']['WlanInterface'],'down'])
        time.sleep(1)
        call(['/sbin/ifconfig', CONFIG['General']['WlanInterface'], 'up'])
        log.info("Restarted wlan")
        time.sleep(DELAY_AFTER_WLAN_RESTART)

        # After delay, get the status again
        status = wpa_get_status()
        if status.get('wpa_state'):
            wpa_state = status['wpa_state']
    else:
        log.info("Restart of wlan is not needed")

    # Check again, if provisioned exit to stop whole wifi provisioning process
    if (wpa_state == "COMPLETED") and status.get('ip_address'):
        log.info("WiFi has been provisioned, ip addr %s" % status.get('ip_address'))
        data = {}
        data['ip_address'] = status.get('ip_address')
        data['mac_address'] = status.get('address')
        return data

    return None

def enable_wifi():
    """ Enables the wlan interface after stopping AP and Dnsmasq. """
    # To make sure web server gets time to complete REST api call (set_config)
    time.sleep(1)

    # Stop the hostapd AP and dnsmasq
    wap.stop_ap_dnsmasq(True, CONFIG['General']['WlanInterface'])

    # Get the current time
    enable_time=datetime.datetime.now().strftime('%H:%M:%S')

    # Enable the WiFi
    wpa_start_network(WebServer.nw_id)

    return restart_wlan(enable_time)

def unwind_provisioning():
    """ Stop AP, Dnsmasq and clear static IP address."""
    log.info("Found network connectivity, stopping provisioning process.")
    wap.stop_ap_dnsmasq(True, CONFIG['General']['WlanInterface'])
    wlan = CONFIG['General']['WlanInterface']
    sip = "%s/24" % CONFIG['General']['WebServerIp']
    call(['/sbin/ip', 'addr', 'del', sip, 'dev', wlan])

def refresh_credentials(wcr, pw_len):
    """" Called periodically (default every min) to refresh the credentials."""
    #log.debug( "Refreshing credentials...")
    wcr.recreate_web_pw(pw_len)
    WebServer.prev_web_pw_num = WebServer.web_pw_num
    WebServer.web_pw_num = wcr.web_pw_num

def multi_cast_ip(cfg_data):
    """ Multicast the IP address of local device for mobile apps to read it.
        Args:
            cfg_data: JSON, IP address and mac address in json format
        Returns: None
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.bind((ANY,SENDERPORT))
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)
    log.info("Started multicast data=%s" % json.dumps(cfg_data))
    cnt = 1
    while (cnt <= MCAST_TRIES):
        try:
            sock.sendto(json.dumps(cfg_data), (MCAST_ADDR,MCAST_PORT) );
            #log.debug("Started multicast cnt=%d" % cnt)
            time.sleep(2)
            cnt = cnt + 1
        except Exception, e:
            # Log the error and keep retrying
            log.error("ERROR: Multi-cast failed: %s" % str(e))
            time.sleep(2)
            cnt = cnt + 1

    log.info("Woluxi multicast Done.")

def init_keypair(path=CONFIG_DIR):
    """ Create the RSA-DER based public-private key.
        Args:
            path: Path of the woluxi config directory where device private key
                  is stored.
        Returns: None
    """
    #log.debug("initializing keypairs")
    if(os.path.isfile(path+'/priv') & os.path.isfile(path+'/pub')) :
        with open(path+'/priv','r') as priv:
            WebServer.prKey = RSA.importKey(priv.read()).exportKey('DER')
        with open(path+'/pub','r') as pub:
            WebServer.pubKey = RSA.importKey(pub.read()).exportKey('DER')
    else:
        # Generate RSA public-private key pair
        key = RSA.generate(1024)
        WebServer.prKey = key.exportKey('DER')
        WebServer.pubKey = key.publickey().exportKey('DER')

        with open(path+'/priv','w+') as priv:
            priv.write(key.exportKey('DER'))
        os.chmod(path+'/priv', 0o400)

        with open(path+'/pub','w+') as pub:
            pub.write(key.publickey().exportKey('DER'))
        os.chmod(path+'/pub', 0o400)
    #log.debug("Public Key:" + base64.b64encode(WebServer.pubKey))

