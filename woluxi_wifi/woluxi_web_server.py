#!usr/bin/env python
'''
    Copyright (c) 2017 Woluxi
    File name: woluxi_web_server.py
    Author: Sandeep Nirmale
    Date created: 4/10/2017
    Date last modified: 8/25/2017
    Python Version: 2.7
    Description: This is based on the BaseHTTPServer and implements the webservice protocol 
          for authenticating incoming requests.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import time
import sys
import BaseHTTPServer
import json
import os
import threading
import subprocess
import hashlib
import base64
import binascii
import errno
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto.Cipher import PKCS1_v1_5 as pkcscipher
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from Crypto.Hash import SHA
from Crypto import Random
from base64 import b64decode
from subprocess import call
from subprocess import check_output
import wpa_wrapper_lib
from wpa_wrapper_lib import *
import logging
import logging.handlers

log = logging.getLogger('wifi_provision')
WLAN = "wlan0"
CIPHER_MODE = AES.MODE_CFB
KEY_LEN = 16
ERROR_TIMEOUT = 10

class WebServer(BaseHTTPServer.BaseHTTPRequestHandler):
    CMD_SET_CONFIG = 'set_config'
    CMD_SET_RECONFIG = 'set_reconfig'
    CMD_GET_PUBLIC_KEY= 'get_public_key'
    CMD_GET_INFO= 'get_info'
    web_pw_num = None
    ap_pw_num = None
    web_pw_len = 0
    default_user_password = None
    prKey = None
    pubKey = None
    prev_web_pw_num = None
    used_prev_pw = False

    # Global settings for main thread
    nw_id = -1
    wifi_configured = False

    def enable_ssh(self):
        """ Enables the ssh interface.
            Args: None
            Returns: None
            Raises: None
        """

        call(['/bin/systemctl', 'enable', 'ssh.service'])
        call(['/bin/systemctl', 'start', 'ssh.service'])
        log.info("Enabled SSH")

    def change_user_password(self, pw):
        """ Changes the password for 'pi' user
            Args:
                pw: String, password of 'pi' user to be set
            Returns: None
            Raises: None
            Errors:
                If password is empty, it logs the error and empty password
                will not be set.
        """

        if len(pw) < 1:
            log.error("ERROR: user password too small")
            return

        cmd = "/bin/echo -e \"%s\\n%s\" | /usr/bin/passwd pi" % (pw,pw)
        out = check_output(cmd, shell=True)
        log.info("Changed pi user password")

    def decrypt_msg(self, edata):
        """ Decrypts the message received from set_config or set_reconfig
            APIs. There is two level of decryption here. First secret key is
            obtained using known device private key. Then secret key is used
            to decrypt the data payload from the message.
            Args:
                edata: Json, request received from rest api
            Returns:
                JSON, decrypted data payload in json format is returned.
            Raises: None
        """

        #log.debug("POST: Private Key:" +    base64.b64encode(WebServer.prKey))
        # Decrypt the secret key using private key
        prKeyObj = RSA.importKey(WebServer.prKey)
        e_sec_key = edata['secret_key']
        #log.debug('e_sec_key' + str(e_sec_key))
	if edata.get('os') and 'ios' in edata['os']:
            dsize = SHA.digest_size
            sentinel = Random.new().read(15+dsize)
            cipher = pkcscipher.new(prKeyObj)
            sec_key = cipher.decrypt(base64.b64decode(e_sec_key), sentinel)
            #log.debug('sec_key ' + sec_key)
            sec_key = base64.b64decode(sec_key)
        else:
            sec_key = prKeyObj.decrypt(base64.b64decode(e_sec_key))
        #log.debug('sec_key' + str(sec_key))


        # Decrypt the data using secret key
        dec_data = self.aes_decrypt(edata['data'], sec_key)
        #log.debug('decrypted data is ' + str(dec_data))
        return (json.loads(str(dec_data)), dec_data)

    def set_reconfig(self, jdata, path = '/etc/woluxi.d'):
        """ Reconfigure WiFi
            Args:
                jdata: JSON, Json request recevied through rest API
                path: String, location where security keys are stored
            Returns: None
            Raises: None
            Errors:
                Error is logged and returned if signature or public_key_id is missing
                in request.
                Error is logged and returned if signature verification fails.
        """

        #log.debug(json.dumps(jdata, indent=4, sort_keys=True))

        if not jdata.get('signature') or not jdata.get('public_key_id'):
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "Signature or key id is missing")
            return

        #log.debug("signature=%s key-id=%s" % (jdata['signature'], jdata['public_key_id']))
        sp_pub_key = None
        with open(path+'/authorized_key', 'r') as file:
            key64 = file.read()
            sp_pub_key = RSA.importKey(key64)

        (data, data_string) = self.decrypt_msg(jdata)
        #verify signature
        signature = jdata['signature']
        h = SHA256.new(data_string)
        verifier = PKCS1_v1_5.new(sp_pub_key)
        if verifier.verify(h, b64decode(signature)):
            return self.start_configuration(data)
        else:
            log.error("Authentication failed")
            time.sleep(ERROR_TIMEOUT)
            self.set_error(403, "Could not verify Signature")

    def save_authorized_key(self, pkey, path = '/etc/woluxi.d'):
        """ Save the smartphone public key
            Args:
                pkey: String, public key of smartphone
                path: String, location where public key is stored.
            Returns: None
        """
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                log.error("ERROR: Failed to save phone key.")
                return

        with open(path+'/authorized_key', 'w') as file:
            file.write('-----BEGIN PUBLIC KEY-----\n')
            file.write(pkey)
            file.write('\n-----END PUBLIC KEY-----\n')
        os.chmod(path+'/authorized_key', 0o400)

    def set_config(self, jdata):
        """ Set the WiFi configuration 
            Args:
                jdata: JSON, Json request received from rest api.
            Returns: None
            Raises: None
            Errors:
                Error 400 returned if keys missing in json
                Error 401 returned if authentication fails due to token mismatch
        """
        if not jdata.get('data') or not jdata.get('secret_key'):
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "Keys missing")
            return

        (data, data_string) = self.decrypt_msg(jdata)

        spPublicKey = data['public_key']
        #log.debug('smartphone public key is' + spPublicKey)
        self.save_authorized_key(spPublicKey)

        if not data.get('led'):
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "LED token missing")
            return

        if (WebServer.used_prev_pw):
            pw = WebServer.prev_web_pw_num
        else:
            pw = WebServer.web_pw_num

        led_token = self.create_padded_key(pw, WebServer.web_pw_len)
        rcvd_token = binascii.a2b_base64(data['led'])
        #log.debug("rcvd_token:" + repr(rcvd_token))

        if rcvd_token != led_token:
            log.error("the received token does not match the transmitted.")
            time.sleep(ERROR_TIMEOUT)
            self.set_error(401, "Auth Failed led token mismatch")
            return
        return self.start_configuration(data)

    def start_configuration(self, data):
        """ Apply configuration recevied from rest api
            Args:
                data: JSON, Json request received from rest api.
            Returns: None
            Raises: None
            Errors:
                Http error 400 returned if keys missing in json or password is empty
        """

        wifi = data.get('wifi')
        #log.debug("got data" + str(wifi))
        if (not wifi) or (not wifi.get('ssid')) or ('password' not in wifi):
            log.error("unable to read ssid or password")
            self.set_error(400, "Unable to read ssid or password")
            return

        pwlen = len(wifi['password'])
        if pwlen > 0 and    pwlen < 8:
            log.error("password length too small")
            self.set_error(400, "Password length too small")
            return

        if 'pi_password' in data:
            self.change_user_password(data['pi_password'])

        if 'ssh_enable' in data:
            self.enable_ssh()

        # Now check the status of wlan connection
        status = wpa_get_status()

        # Send http response as OK before sending json response
        self.set_response()

        out = {}
        out['status'] = 'OK'
        if status.get('ssid'):
            out['ssid'] = status['ssid']
        if status.get('status'):
            out['wpa_state'] = status['wpa_state']
        if status.get('address'):
            out['mac_address'] = status['address']

        # Add wifi credentials using wpa_cli based wrapper lib
        log.info("Setting configuration with ssid=%s" % wifi['ssid'])
        nwid = wpa_configure_network(wifi['ssid'], wifi['password'])
        if nwid < 0:
            log.error("Unable to configure WiFi")
            self.set_error(400, "Unable to configure WiFi for ssid %s" % wifi['ssid'])
            return

        #log.debug("WiFi settings configured: ssid=%s    pw=%s" % (wifi['ssid'], wifi['password']))
        self.wfile.write(json.dumps(out))
        WebServer.wifi_configured = True
        WebServer.nw_id = nwid
        return

    def create_padded_key(self, wpw, nbytes):
        """ Create a key
            Args:
                wpw: String, password of web server
                nbytes: Integer, length of password
            Returns: key in bytearry format
        """
        apw = WebServer.ap_pw_num
        plen = WebServer.web_pw_len
        stuffed = 0
        ba = bytearray(b'')

        if apw != None:
            num = wpw | (apw << 8*plen)
            plen += 1
        else:
            num = wpw

        while (stuffed < nbytes):
            for i in range(0, plen):
                shift = plen - i -1
                mask = 0xff << (shift*8)
                byte = (num & mask) >> (shift*8)
                ba.append(byte)
                stuffed += 1
                if stuffed >= nbytes:
                    break
        return ba

    def _unpad(self, s):
        """ Unpadding routine for decryption"""
        return s[:-ord(s[len(s)-1:])]

    def aes_decrypt(self, enc_data, key):
        """ Changes the password for 'pi' user
            Args:
                enc_data: encrypted data
                key: key used to encrypt the data
            Returns: String, Decreypted message
            Raises: None
        """
        enc = base64.b64decode(enc_data)
        iv = enc[:16]
        cipher = AES.new(bytes(key), CIPHER_MODE, iv, segment_size=128)
        msg = self._unpad(cipher.decrypt( enc[16:]))
        #log.debug("Decrypted Msg:%s" % repr(msg))
        return msg

    def get_public_key(self, data):
        """ Rest API to get the public key of device
            Args:
                data: JSON, Json format request recevied from rest api
            Returns: None
            Raises:
                Exception ValueError is raised if challenge is not matched or
                found first time.
            Errors:
                Http error 401 returned if challenge does not match after
                retries.
        """
        if not data.get('data'):
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "Missing payload")
            return

        pw = WebServer.web_pw_num
        first_time = True
        while True:
            led_key = self.create_padded_key(pw, KEY_LEN)
            #log.debug("KEY:" + str(led_key))
            #log.debug(''.join('{:02x}'.format(x) for x in led_key))
            msg = self.aes_decrypt(data['data'], led_key)
            #log.debug("message is " + str(msg))

            try:
                #log.debug("Inside try %d" % pw)
                json_msg = json.loads(msg)
                if 'challenge' not in json_msg:
                    raise ValueError('Cound not find challenge')
                break
            except ValueError:
                # If this is first time then try previous credentials
                if (not WebServer.used_prev_pw) and (WebServer.prev_web_pw_num != None):
                    log.info("Checking with previous cycle credentials")
                    pw = WebServer.prev_web_pw_num
                    WebServer.used_prev_pw = True
                    continue
                log.error("ERROR: Incorrect credentials")
                WebServer.used_prev_pw = False
                time.sleep(ERROR_TIMEOUT)
                self.set_error(401, "Auth Failed - Could not verify challenge.")
                return
            except:
                e = sys.exc_info()[0]
                log.error("ERROR: Unexpected error: %s" % e)
                time.sleep(ERROR_TIMEOUT)
                self.set_error(401, "Auth Failed - Could not verify challenge.")
                return

        challenge = json_msg.get('challenge')
        #log.debug("challenge:" + challenge)
        hash_object = hashlib.sha256(challenge.encode('utf-8'))
        hex_str = hash_object.hexdigest()
        #log.debug("Private Key:" +    base64.b64encode(WebServer.prKey))

        # Send http response as OK before sending json response
        self.set_response()

        out = {}
        out['status'] = "OK"
        out['challenge_answer'] = hex_str
        out['public_key'] = base64.b64encode(WebServer.pubKey)    ## Check base64 pub key
        #log.debug(json.dumps(out, indent=4, sort_keys=True))
        self.wfile.write(json.dumps(out))
        return

    def get_info(self, data):
        """ Gets the device information including version """
        # Send http response as OK before sending json response
        self.set_response()

        out = {}
        out['status'] = "OK"
        out['version'] = "1.0"
        self.wfile.write(json.dumps(out))
        return

    # This routine is called when error happens when processing API requests
    def set_error(self, error, reason):
        """ Sets and respond with given http error code. """
        out = {}
        out['status'] = 'FAIL'
        out['status_code'] = error
        out['reason'] = reason
        jsonout = json.dumps(out)
        self.send_response(error, reason)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Content-Length', len(jsonout))
        self.end_headers()
        self.wfile.write(jsonout)

    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        """ Handles GET type requests """
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))
        jdata = json.loads(self.data_string)

        # Validate the GET request
        if not jdata.get('cmd'):
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400)
            self.end_headers()
            return

        if jdata['cmd'] == self.CMD_GET_INFO:
            self.get_info(jdata)
        else:
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "Invalid Request: Unknown command")

        time.sleep(ERROR_TIMEOUT)
        self.end_headers()
        return

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_POST(self):
        """ Handles POST type requests """
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))
        jdata = json.loads(self.data_string)

        #log.debug("In POST: %s" % WebServer.web_pw_num)

        # Validate the POST request
        if not jdata.get('cmd'):
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "Invalid Request: Could not find cmd")
            self.end_headers()
            return

        #log.debug("input is " + json.dumps(jdata, indent=4, sort_keys=True))

        if jdata['cmd'] == self.CMD_SET_CONFIG:
            self.set_config(jdata)
        elif jdata['cmd'] == self.CMD_SET_RECONFIG:
            self.set_reconfig(jdata)
        elif jdata['cmd'] == self.CMD_GET_PUBLIC_KEY:
            self.get_public_key(jdata)
        else:
            time.sleep(ERROR_TIMEOUT)
            self.set_error(400, "Invalid Request: Unknown command")

        return
