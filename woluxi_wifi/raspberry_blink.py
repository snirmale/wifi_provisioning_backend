#!/usr/bin/env python
'''
    Copyright (c) 2017 Woluxi
    File name: xmit.py
    Author: Ninad Ghodke
    Date created: 10/11/2017
    Python Version: 2.7
    Description: This module transmits the data via the LED according to the protocol.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from xmit import BitBlink

class RaspberryBlink(BitBlink):
    """ This class is for Most Raspberry Pi except Pi Zero."""
    def __init__(self):
        """ Open the file to control LED on/off. """
        self.fdled = open('/sys/class/leds/led0/brightness', 'w')

    def blink(self, inp):
        """ Turn LED on or off given the input. """
        if (inp in [True, '1']) :
            self.fdled.write('1\n')
            self.fdled.flush()
        else :
            self.fdled.write('0\n')
            self.fdled.flush()

