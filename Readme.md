# Woluxi WiFi Provisioning Backend for Raspberry Pi

Woluxi headless Wifi provisioning mechanism is a simple and secure way of configuring WiFi for IoT devices like Raspberry Pi.
* It does not need keyboard or monitor connected to your IoT device. 
* It uses proof of presence mechanism for authentication. 
* Woluxi's patented technique enables Raspberry Pi device to communicate credentials with the SmartPhone by simply blinking LED light on it.
* It eliminates the need for hardcoded default passwords.
* Woluxi WiFi provisioning framework consists of 
    * python based backend (runs on Pi device) and
    * smartphone application (Android or iOS).

Once Woluxi software is installed on Raspberry Pi, following benefits are available:
* Securely provisioning of WiFi credentials into Pi device without any extra hardware or infrastructure
* Ability to change the 'pi' user password
* Enable ssh at same time as WiFi provisioning

Currently one can take benefit of this only after installing Woluxi WiFi provisioning backend on Raspberry Pi. At present, it does not handle the case of WiFi provisioning on a vanilla install as one might need network connectivity to install Woluxi software.

RASPBIAN OS Support: [Jessie and Stretch] (http://www.woluxi.com/deb/wifiprovisioning_0.2_all.deb)

Mobile Apps Support: [Android] (https://play.google.com/store/apps/details?id=com.woluxi.scannerlibdemo) and [iOS] (https://itunes.apple.com/us/app/woluxi-wifi-provisioning-pi/id1313026344)

## Getting Started

You would need the Woluxi Wifi Provisioning Apps on your Android or iOS phones. Check them on respective app store.

Start the app, enter the WiFi details that you would want your Raspberry-Pi to connect too and in the next screen point towards the flashing LED light next to the power LED.

### Prerequisites

dnsmasq, hostapd, python-crypto, wpasupplicant, iw, net-tools, systemd

## Deployment

This is a python based app, you would need to install the pre-reqs mentioned above and install it as a daemon.

### Download prebuilt debian file OR Creating debian file

Download pre-built debian file from here: http://www.woluxi.com/deb/wifiprovisioning_0.2_all.deb

OR

Build debian file using following command after you downloaded source code from gitlab (run from top level directory).

**sudo dpkg-buildpackage -b**

### Installing Woluxi debian file

**sudo dpkg -i ./wifiprovisioning_0.1_all.deb**

If prerequisites are installed, the above command should successfully install. If not, it will fail with the error asking for packages mentioned in the prerequisites above. then run following few commands to complete the install.

```
pi@raspberrypi:~ $ sudo dpkg -i wifiprovisioning_0.2_all.deb
Selecting previously unselected package wifiprovisioning.
(Reading database ... 122683 files and directories currently installed.)
Preparing to unpack wifiprovisioning_0.2_all.deb ...
Unpacking wifiprovisioning (0.2) ...
dpkg: dependency problems prevent configuration of wifiprovisioning:
 wifiprovisioning depends on dnsmasq; however:
  Package dnsmasq is not installed.
 wifiprovisioning depends on hostapd; however:
  Package hostapd is not installed.

dpkg: error processing package wifiprovisioning (--install):
 dependency problems - leaving unconfigured
Errors were encountered while processing:
 wifiprovisioning
```

**sudo apt-get update**

```
pi@raspberrypi:~ $ sudo dpkg -i wifiprovisioning_0.2_all.deb
Selecting previously unselected package wifiprovisioning.
(Reading database ... 122683 files and directories currently installed.)
Preparing to unpack wifiprovisioning_0.2_all.deb ...
Unpacking wifiprovisioning (0.2) ...
dpkg: dependency problems prevent configuration of wifiprovisioning:
 wifiprovisioning depends on dnsmasq; however:
  Package dnsmasq is not installed.
 wifiprovisioning depends on hostapd; however:
  Package hostapd is not installed.

dpkg: error processing package wifiprovisioning (--install):
 dependency problems - leaving unconfigured
Errors were encountered while processing:
 wifiprovisioning
```

**sudo apt-get install -f**

```
pi@raspberrypi:~ $ sudo apt-get install -f
Reading package lists... Done
Building dependency tree
Reading state information... Done
Correcting dependencies... Done
The following additional packages will be installed:
  dns-root-data dnsmasq dnsmasq-base hostapd libnl-route-3-200
The following NEW packages will be installed:
  dns-root-data dnsmasq dnsmasq-base hostapd libnl-route-3-200
0 upgraded, 5 newly installed, 0 to remove and 127 not upgraded.
1 not fully installed or removed.
Need to get 982 kB of archives.
After this operation, 2,474 kB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://mirrordirector.raspbian.org/raspbian stretch/main armhf libnl-route-3-200 armhf 3.2.27-2 [113 kB]
Get:2 http://archive.raspberrypi.org/debian stretch/main armhf dnsmasq-base armhf 2.76-5+rpt1+deb9u1 [385 kB]
...
...
Setting up hostapd (2:2.4-1+deb9u1) ...
Setting up dnsmasq (2.76-5+rpt1+deb9u1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/dnsmasq.service → /lib/systemd/system/dnsmasq.service.
Setting up wifiprovisioning (0.2) ...
Processing triggers for systemd (232-25+deb9u1) ...
```

### Enabling Woluxi WiFi provisioning service

**sudo systemctl enable wifiprovisioning.service**
```
pi@raspberrypi:~ $ sudo systemctl enable wifiprovisioning.service
Created symlink /etc/systemd/system/multi-user.target.wants/wifiprovisioning.service → /lib/systemd/system/wifiprovisioning.service.
```

**sudo systemctl enable wifiprovisioning.service**
```
pi@raspberrypi:~ $ sudo systemctl enable wifiprovisioning.service
... raspberrypi systemd[1]: Started Wifi Provisioning solution provided by Woluxi.
```

## Authors

* **Sandeep Nirmale** - *Initial work* - [Woluxi](http://www.woluxi.com)
* **Ninad Ghodke** - *Initial work* - [Woluxi](http://www.woluxi.com)

## License

This project is licensed under the GPL License - see the [LICENSE](LICENSE) file for details

## Description

This backend software consists of following component:
* Main (wifi_provisioning.py) - Main control program
* Credential Generation (woluxi_credential.py) - This is responsible for creating crendetials for AP and web server.
* Access Point (woluxi_ap.py) - Starting and stopping of access point and dnsmasq service
* Woluxi Web Server (woluxi_web_server.py) - Woluxi web server which handles REST APIs
* WPA CLI wrapper (wpa_wrapper_lib.py) - Wrapper APIs for using wpa_cli programmatically
* Xmit module (xmit.py) - A module to transmit data using LED blinks
* Config (config.py) - This handles defaults and woluxi config file
